/**
* @file     main.cpp
* @brief    Programa que gerencia alguns dados de empresas
* @author   Pedro Emerick (p.emerick@live.com)
* @since    03/05/2017
* @date	    06/05/2017
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <ctime>

#include "empresa.h"
#include "menus.h"
#include "funcs_dados.h"

/** 
 * @brief Função principal
 */
int main ()
{
    Empresa *emp = NULL;
    int num_empresa = 0; 

    time_t timer;
    struct tm *data;
    
    time(&timer); // Obtem informações de data e hora
    data = localtime( &timer ); // Converte a hora atual para a hora local

    int menu = -1;
    
    emp = carrega_empresa (emp, &num_empresa);

    while (menu != 0) {

        menu = menu_principal (menu);

        if (menu == 1)
        {
            emp = nova_empresa (emp, &num_empresa);
            cadastra_empresa (emp, num_empresa);
        }
        else if (menu == 2)
        {
            add_funcionario (emp, num_empresa, data);
        }
        else if (menu == 3) 
        {
            /** Lista todos os dados da empresa e dos funcionarios */
            int indice_emp = busca_empresa (emp, num_empresa);

            if (indice_emp != -1)
            {
                cout << endl;
                if (emp[indice_emp].getNumEmpregados () == 0)
                    cout << "Não existem funcionarios cadastrados nesta empresa !!!" << endl << endl;
                else {
                    cout << emp[indice_emp];
                }
            }      
        }
        else if (menu == 4)
        {
            /** Recebe a porcentagem para aumento do salario dos funcionarios */
            int indice_emp = busca_empresa (emp, num_empresa);

            if (indice_emp != -1)   
            {
                float porcentagem;

                cout << "Deseja fazer um aumento de quanto por cento para os funcionários: ";
                cin >> porcentagem;

                emp[indice_emp].setAumento (porcentagem);

                cout << "Aumento realizado !!!" << endl;
            }
        }
        else if (menu == 5) 
        {
            /** Determina o dia, mes, e ano quando iniciado o programa e lista os funcionarios em tempo
                de experiencia */
            int dia, mes, ano;

            dia = data->tm_mday;
            mes = data->tm_mon + 1;
            ano = data->tm_year + 1900;

            int indice_emp = busca_empresa (emp, num_empresa);
            
            if (indice_emp != -1)
            {
                emp[indice_emp].FuncionarioExperiencia (dia, mes, ano);
            }
        }
        else if (menu == 6)
        {
            emp = carrega_empresa (emp, &num_empresa);
        }
        else if (menu < 0 || menu > 6)
        {
            cout << "Opção inválida !!! Digite novamente." << endl;
        }
    }

    for (int ii = 0; ii < num_empresa; ii++)
    {
        emp[ii].Salvar ();
    }

    delete [] emp;

    return 0;
}