/**
 * @file	funcionario.cpp
 * @brief	Implementacao dos metodos da classe Funcionario
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	03/05/2017
 * @date	06/05/2017
 * @sa		funcionario.h
 */

#include <iostream>
using std::endl;
using std::cout;

#include <ostream>
using std::ostream;

#include "funcionario.h"

/**
 * @details Os valores de nome é iniciado com espaço, e salario, dia, mes e ano sao inicializados com zero
  */
Funcionario::Funcionario()
{
    nome = " ";
    salario = 0;
    dia = 0;
    mes = 0;
    ano = 0;
}

/**
 * @details Os valores dos dados dos funcionarios sao recebidos por parametro
 * @param   n String para o nome
 * @param   s Valor para o salario
 * @param   d Valor para o dia
 * @param   m Valor para o mes
 * @param   a Valor para o ano
 */
Funcionario::Funcionario(string n, float s, int d, int m, int a)
{
    nome = n;
    salario = s;
    dia = d;
    mes = m;
    ano = a;
}

/**
 * @return Nome do funcionario 
 */
string Funcionario::getNome()
{
    return nome;
}

/**
 * @details O metodo modifica o nome do funcionario
 * @param   n Nome para o funcionario 
 */
void Funcionario::setNome(string n)
{
    nome = n;
}

/**
 * @return Salario do funcionario 
 */
float Funcionario::getSalario()
{
    return salario;
}

/**
 * @details O metodo modifica o salario do funcionario
 * @param   s Salario para o funcionario 
 */
void Funcionario::setSalario(float s)
{
    salario = s;
}

/**
 * @return Dia de admissao do funcionario 
 */
int Funcionario::getDia()
{
    return dia;
}

/**
 * @details O metodo modifica o dia de admissao do funcionario
 * @param   d Dia de admissao do funcionario 
 */
void Funcionario::setDia(int d)
{
    dia = d;
}

/**
 * @return Mes de admissao do funcionario 
 */
int Funcionario::getMes()
{
    return mes;
}

/**
 * @details O metodo modifica o mes de admissao do funcionario
 * @param   m Mes de admissao do funcionario 
 */
void Funcionario::setMes(int m)
{
    mes = m;
}

/**
 * @return Ano de admissao do funcionario 
 */
int Funcionario::getAno()
{
    return ano;
}

/**
 * @details O metodo modifica o ano de admissao do funcionario
 * @param   a Ano de admissao do funcionario 
 */
void Funcionario::setAno(int a)
{
    ano = a;
}

/** 
 * @details O operador e sobrecarregado para representar um funcionario 
 *			com os dados do funcionario
 * @param	os Referencia para stream de saida
 * @param	f Referencia para o objeto Funcionario a ser impresso
 * @return	Referencia para stream de saida
 */
ostream &operator<<(ostream &os, Funcionario &f)
{
    os << "Nome: " << f.nome << endl;
    os << "Salario: R$ " << f.salario << endl;
    os << "Data de admissao: " << f.dia << "/" << f.mes << "/" << f.ano << endl << endl;

    return os;
}

/** 
 * @details O metodo verifica se o funcionario esta em tempo de experiencia
 * @param   d Dia atual
 * @param   m Mes atual
 * @param   a Ano atual
 * @return	Se o funcionario está em tempo de experiencia ou não
 */
bool Funcionario::Experiencia(int d, int m, int a)
{

    int dias_mes[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    int aux = dia + (mes * dias_mes[mes - 1]) + (ano * 365);
    int aux2 = d + (m * dias_mes[m - 1]) + (a * 365);

    if (aux2 - aux < 90)
        return true;

    return false;
}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao de igualdade
 *			de um instante de funcionario a outro
 * @param	f Instante de funcionario que sera atribuido ao objeto que invoca
 *			o metodo
 * @return	Se um instante é igual ao outro
 */
bool Funcionario::operator==(Funcionario const f)
{

    if (nome == f.nome && salario == f.salario && dia == f.dia && mes == f.mes && ano == f.ano)
        return true;

    return false;
}