/**
 * @file	empresa.cpp
 * @brief	Implementacao dos metodos da classe Empresa
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	03/05/2017
 * @date	06/05/2017
 * @sa		empresa.h
 */

#include "funcionario.h"

#include <iostream>
using std::cout;
using std::endl;

#include <ostream>
using std::ostream;

#include <fstream>
using std::ofstream;

#include <string>
using std::string;

#include "empresa.h"

/**
 * @details Os valores de nome é iniciado com espaço, cnpj iniciado com zero, empregados iniciado com vazio 
 *			e o numero de empregados com zero
 */
Empresa::Empresa () {
    nome = " ";
    cnpj = 0;
    empregados = NULL;
    num_empregados = 0;
}

/**
 * @return Nome da empresa 
 */
string Empresa::getNomeEmpresa () {
    return nome;
}

/**
 * @details O metodo modifica o nome da empresa
 * @param   n Nome para a empresa 
 */
void Empresa::setNomeEmpresa (string n){
    nome = n;
}

/**
 * @return Cnpj da empresa 
 */
long int Empresa::getCnpj () {
    return cnpj;
} 

/**
 * @details O metodo modifica o cnpj da empresa
 * @param   c Cnpj para a empresa 
 */
void Empresa::setCnpj (long int c) {
    cnpj = c;
}

/**
 * @return Numero de empregados da empresa 
 */
int Empresa::getNumEmpregados () {
    return num_empregados;
}

/**
 * @details O metodo aumenta o vetor de empregados salvando os anteriores 
            e colocando de volta no vetor
 * @param   n Novo numero de empregados para a empresa 
 */
void Empresa::setNovoFuncionario (int n) {
    
    if (num_empregados > 0)
    {
        Funcionario *empregados_temp = new Funcionario [num_empregados];
    
        for (int jj = 0; jj < num_empregados; jj++) {
            empregados_temp [jj].setNome (empregados[jj].getNome ());
            empregados_temp [jj].setSalario (empregados[jj].getSalario ());
            empregados_temp [jj].setDia (empregados[jj].getDia ());
            empregados_temp [jj].setMes (empregados[jj].getMes ());
            empregados_temp [jj].setAno (empregados[jj].getAno ());
        }

        delete [] empregados;

        empregados = new Funcionario [n];

        for (int jj = 0; jj < num_empregados; jj++) {
            empregados [jj].setNome (empregados_temp[jj].getNome ());
            empregados [jj].setSalario (empregados_temp[jj].getSalario ());
            empregados [jj].setDia (empregados_temp[jj].getDia ());
            empregados [jj].setMes (empregados_temp[jj].getMes ());
            empregados [jj].setAno (empregados_temp[jj].getAno ());
        }

        delete [] empregados_temp;
    }
    else 
    {
        delete [] empregados;

        empregados = new Funcionario [n];
    }

    num_empregados = n;
}

/**
 * @details O metodo inseri os dados de um funcionario
 * @param   n Nome para o funcionario 
 * @param   s Salario para o funcionario
 * @param   d Dia de admissao do funcionario
 * @param   m Mes de admissao do funcionario
 * @param   a Ano de admissao do funcionario
 */
void Empresa::setEmpregados (string n, float s, int d, int m, int a) {

    if (ExisteFuncionario (n, s, d, m, a) == true)
    {
        cout << "Funcionario já cadastrado !!!" << endl;
        
        return;
    }
    else
    {
        int aux = num_empregados + 1;
        int temp = num_empregados;
        
        Empresa::setNovoFuncionario (aux);

        empregados[temp].setNome (n);
        empregados[temp].setSalario (s);
        empregados[temp].setDia (d);
        empregados[temp].setMes (m);
        empregados[temp].setAno (a);
    }
}

/** 
 * @details O operador e sobrecarregado para representar uma empresa 
 *			com os dados da empresa e dos funcionarios da empresa
 * @param	os Referencia para stream de saida
 * @param	e Referencia para o objeto Empresa a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream &os, Empresa &e) {
    
    os << "Dados da Empresa: " << endl;
    os << "Nome: " << e.nome << endl;
    os << "CNPJ: " << e.cnpj << endl << endl;

    os << "Lista de funcionários:" << endl << endl;

    for (int ii = 0; ii < e.num_empregados; ii++)
    {
        os << e.empregados[ii];
    }

    return os;
}


/**
 * @details O metodo modifica o salario dos funcionarios com um aumento
 * @param   p Porcentagem para aumento no salario dos funcionários
 */
void Empresa::setAumento (int p) {

    double aux = (double) p / (double) 100;

    for (int ii = 0; ii < num_empregados; ii++)
    {
        float temp = empregados[ii].getSalario ();
        float novo_salario = (temp * aux) + temp;
        
        empregados[ii].setSalario (novo_salario);
    }
}

/**
 * @details O metodo imprimi os funcionarios em experiência
 * @param   d Dia atual
 * @param   m Mes atual
 * @param   a Ano atual
 */
void Empresa::FuncionarioExperiencia (int d, int m, int a) {

    int cont = 0;

    for (int ii = 0; ii < num_empregados; ii++)
    {
        if (empregados[ii].Experiencia (d, m, a) == true)
            cout << empregados[ii];
        else 
            cont += 1;
    }

    if (cont == num_empregados)
        cout << "Nenhum funcionario em tempo de experiencia !!!" << endl;
}

/**
 * @details O metodo salva as empresa em um arquivo de saida
 */
void Empresa::Salvar () {

    string nome_arq = "./data/output/" + nome + ".csv";
    ofstream arquivo (nome_arq);

    arquivo << nome << endl;
    arquivo << cnpj << endl;
    arquivo << num_empregados << endl;

    for (int ii = 0; ii < num_empregados; ii++) 
    {
        arquivo << empregados[ii].getNome () << ";" << empregados[ii].getSalario () << ";"; 
        arquivo << empregados[ii].getDia () << "/" << empregados[ii].getMes () << "/" << empregados[ii].getAno () << endl;
    }

    arquivo.close ();
}

/**
 * @details O metodo verifica se o funcionario ja esta nos dados da empresa
 * @param   n Nome para o funcionario 
 * @param   s Salario para o funcionario
 * @param   d Dia de admissao do funcionario
 * @param   m Mes de admissao do funcionario
 * @param   a Ano de admissao do funcionario
 */
bool Empresa::ExisteFuncionario (string n, float s, int d, int m, int a) {

    Funcionario temp (n, s, d, m, a);

    for (int jj = 0; jj < num_empregados; jj++)
    {
        if (temp == empregados[jj])
            return true;
    }

    return false;
}


/** 
 * @details O operador e sobrecarregado para realizar a atribuicao
 *			de um instante de empresa a outra
 * @param	e Instante de empresa que sera atribuido ao objeto que invoca
 *			o metodo
 * @return	Autoreferencia para o objeto que invoca o metodo, com
 *			atributos atualizados
 */
Empresa& Empresa::operator = (Empresa const &e) {

    nome = e.nome;
    cnpj = e.cnpj;

    for (int ii = 0; ii < e.num_empregados; ii++)
    {
        Empresa::setEmpregados (e.empregados[ii].getNome (), e.empregados[ii].getSalario (), e.empregados[ii].getDia (), e.empregados[ii].getMes (), e.empregados[ii].getAno ());
    }

    return *this;
}

/**
 * @details O vetor de empregados é liberado se alocado com valores maiores que zero
 */
Empresa::~Empresa () {
    if (num_empregados > 0)
        delete [] empregados;
}