/**
* @file     menus.cpp
* @brief    Implementacao das funcoes que imprimem os menus do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    03/05/2017
* @date	    06/05/2017
*/

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include "menus.h"

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @param menu Valor do menu
* @return Opcao escolhida pelo usuario
*/
int menu_principal (int menu)
{
    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                  Menu Principal                   *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Criar empresa                             *" << endl;
    cout << "*      2) Adicionar funcionários a empresa          *" << endl;
    cout << "*      3) Listar dados e funcionários da empresa    *" << endl;
    cout << "*      4) Aumento para funcionários                 *" << endl;
    cout << "*      5) Listar funcionários em experiência        *" << endl;
    cout << "*      6) Carregar empresa                          *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Sair                                      *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção desejada:" << endl << "--> ";
    cin >> menu;
    cout << endl;

    return menu;
}

/**
* @brief Função que imprime o menu para a adicao de funcionarios com suas opcoes e recebe a escolha do usuario
* @param menu2 Valor do menu para a adicao de funcionarios
* @return Opcao escolhida pelo usuario
*/
int menu_add_funcionario (int menu2)
{
    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*      1) Adicionar funcionário pelo programa       *" << endl;
    cout << "*      2) Adicionar funcionário por arquivo         *" << endl;
    cout << "*****************************************************" << endl << endl;            

    cout << "Digite a opção desejada:" << endl << "--> ";
    cin >> menu2;
    cout << endl;

    return menu2;
}

/**
* @brief Função que imprime o menu para carregar uma empresa atraves de um arquivo 
         com suas opcoes e recebe a escolha do usuario
* @param opcao Valor da opcao
* @return Opcao escolhida pelo usuario
*/
int menu_inicial (int opcao)
{
    cout << endl;
    cout << "*******************************************************************************************" << endl;
    cout << "*          Deseja carregar alguma empresa cadastrada anteriormente no programa ?          *" << endl;
    cout << "*                                                                                         *" << endl;
    cout << "*          1) Sim                                                                         *" << endl; 
    cout << "*          2) Não                                                                         *" << endl;
    cout << "*******************************************************************************************" << endl << endl;
    
    cout << "Digite a opção desejada:" << endl << "--> ";
    cin >> opcao;
    cout << endl;

    return opcao;
}