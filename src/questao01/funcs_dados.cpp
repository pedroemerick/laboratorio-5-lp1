/**
* @file     funcs_dados.cpp
* @brief    Implementacao de algumas das funções que modificam os dados do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since    03/05/2017
* @date	    06/05/2017
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

#include <string>
using std::string;

#include <fstream>
using std::ifstream;

#include <cstdlib>
using std::atoi;
using std::atof;

#include <ctime>

#include "empresa.h"
#include "menus.h"

#include <sstream>
using std::ws;

#include "funcs_dados.h"

/**
* @brief Função que busca uma empresa pelo cnpj
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
* @return Indice de onde se encontra a empresa
*/
int busca_empresa (Empresa *emp, int num_empresa) 
{
    int indice_emp;
    long int cnpj_emp;

    cout << "Digite o CNPJ da empresa: ";
    cin >> cnpj_emp;

    int ii;
    for (ii = 0; ii < num_empresa; ii++)
    {
        if (emp[ii].getCnpj () == cnpj_emp) {
            indice_emp = ii;
            break;
        }
    }
    if (ii == num_empresa)
    {
        cout << "Empresa não cadastrada !!!" << endl;

        return -1;
    }

    return indice_emp;
}

/**
* @brief Função que aloca o vetor de empresas com o seu tamanho mais um
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
* @return Vetor de empresas com mais um espaço para empresa
*/
Empresa* nova_empresa (Empresa *emp, int *num_empresa)
{
    int temp = *num_empresa;
    *num_empresa += 1;

    if (temp > 0)
    {
        Empresa *emp_temp = new Empresa [temp];

        for (int jj = 0; jj < temp; jj++) {
            emp_temp [jj] = emp [jj];
        }

        delete [] emp;

        emp = new Empresa [*num_empresa];

        for (int jj = 0; jj < temp; jj++) {
            emp [jj] = emp_temp [jj];
        }

        delete [] emp_temp;
    }
    else 
    {
        delete [] emp;

        emp = new Empresa [*num_empresa];
    }

    return emp;
}

/**
* @brief Função que cria uma nova empresa, recebendo seus dados e atribuindo na empresa
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
*/
void cadastra_empresa (Empresa *emp, int num_empresa)
{
    int temp = num_empresa - 1;

    string nome;
    cout << "Digite o nome da empresa: ";
    getline (cin >> ws, nome);

    cout << "Digite o CNPJ: ";
    long int cnpj;
    cin >> cnpj;

    emp[temp].setNomeEmpresa (nome);
    emp[temp].setCnpj (cnpj);

    cout << "Empresa '" << emp[temp].getNomeEmpresa () << "' criada com sucesso !!!" << endl;
}

/**
* @brief Função que adiciona funcionario em uma empresa pelo terminal ou por um arquivo
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
* @param data Data atual quando iniciado o programa
*/
void add_funcionario (Empresa *emp, int num_empresa, struct tm *data)
{
    int menu2 = 0;
    string nome_f;
    float salario;
    int dia, mes, ano;
    int qntd_add;

    int indice_emp = busca_empresa (emp, num_empresa);

    if (indice_emp == -1)
    {
        return;
    }

    menu2 = menu_add_funcionario (menu2);

    switch (menu2) {
        case 1: {
            cout << "Quantos funcionarios deseja cadastrar: ";
            cin >> qntd_add;

            for (int ii = 0; ii < qntd_add; ii++) {
                cout << endl;
                cout << "Nome do Funcionario: ";
                getline (cin >> ws, nome_f);

                cout << "Salario: ";
                cin >> salario;

                dia = data->tm_mday;
                mes = data->tm_mon + 1;
                ano = data->tm_year + 1900;

                emp[indice_emp].setEmpregados (nome_f, salario, dia, mes, ano);
            }

            cout << "Funcionários cadastrados com sucesso !!!" << endl;

            break;
        }
        case 2: { 
            string nome_arq = "./data/input/";
            string temp;

            cout << "Digite o nome do arquivo com sua extensao que contém os dados dos funcionarios (o arquivo deve estar na pasta ./data/input/): " << endl;
            cout << "Exemplo: teste.csv" << endl;
            getline (cin >> ws, temp);
            
            nome_arq += temp;

            ifstream arquivo (nome_arq);

            if (!arquivo)
            {
                cout << "Arquivo não encontrado !!!" << endl;

                break;
            }

            getline (arquivo, temp);
            qntd_add = atoi (temp.c_str ());

            for (int ii = 0; ii < qntd_add; ii++) {
                getline (arquivo, nome_f, ';');
                getline (arquivo, temp, ';');
                salario = atof (temp.c_str ());
                getline (arquivo, temp, '/');
                dia = atoi (temp.c_str ());
                getline (arquivo, temp, '/');
                mes = atoi (temp.c_str ());
                getline (arquivo, temp);
                ano = atoi (temp.c_str ());

                emp[indice_emp].setEmpregados (nome_f, salario, dia, mes, ano);
            }

            cout << "Funcionários cadastrados com sucesso !!!" << endl;
            
            arquivo.close ();

            break;
        }
        default: {
            cout << "Opção inválida !!!" << endl;

            break;
        }
    }
}

/**
* @brief Função que carrega os dados de uma empresa e os dados dos seus funcionarios atraves de um arquivo
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
*/
Empresa* carrega_empresa (Empresa *emp, int *num_empresa)
{
    int opcao = 0;

    while (opcao != 2)
    {
        opcao = menu_inicial (opcao);

        if (opcao == 1)
        {
            string nome_arq = "./data/output/";
            string nome_emp;

            cout << "Digite o nome da empresa que foi cadastrada: ";
            cin >> nome_emp;

            nome_arq += nome_emp + ".csv";

            ifstream arquivo (nome_arq);

            if (!arquivo)
            {
                cout << "Empresa não encontrada, por favor cadastre-a antes !!!" << endl;

                return emp;
            }

            int temp_i = *num_empresa;
            emp = nova_empresa (emp, num_empresa);

            string temp_s;

            int aux;

            string nome_f;
            float salario;
            int dia, mes, ano;

            getline (arquivo, temp_s);
            emp[temp_i].setNomeEmpresa (temp_s);

            getline (arquivo, temp_s);
            emp[temp_i].setCnpj (atoi (temp_s.c_str ()));

            getline (arquivo, temp_s);
            aux = atoi (temp_s.c_str ());

            for (int ii = 0; ii < aux; ii++)
            {
                getline (arquivo, nome_f, ';');
                getline (arquivo, temp_s, ';');
                salario = atof (temp_s.c_str ());
                getline (arquivo, temp_s, '/');
                dia = atoi (temp_s.c_str ());
                getline (arquivo, temp_s, '/');
                mes = atoi (temp_s.c_str ());
                getline (arquivo, temp_s);
                ano = atoi (temp_s.c_str ());

                emp[temp_i].setEmpregados (nome_f, salario, dia, mes, ano);
            }

            cout << "Empresa '" << emp[temp_i].getNomeEmpresa () << "' carregada com sucesso !!!" << endl;

            opcao = 2;

            arquivo.close ();
        }
        else if (opcao < 1 || opcao > 2)
        {
            cout << "Opção inválida !!! Digite novamente." << endl;
        }
    }

    return emp;
}