/**
 * @file	jogador.cpp
 * @brief	Implementacao dos metodos da classe Jogador
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	07/05/2017
 * @date	07/05/2017
 * @sa		jogador.h
 */

#include "jogador.h"

#include <cstring>
using std::string;

#include <ostream>
using std::ostream;

#include <iostream>
using std::endl;

/**
 * @details Os valores de nome é inicializado com espaço, total com zero, parar e excluido com false
 */
Jogador::Jogador () {
    nome = " ";
    total = 0;
    parar = false;
    excluido = false;
}

/**
 * @return Nome do jogador 
 */
string Jogador::getNome () {
    return nome;
}

/**
 * @details O metodo modifica o nome do jogador
 * @param n Nome para o jogador 
 */
void Jogador::setNome (string n) {
    nome = n;
}

/**
 * @return Total de pontos do jogador 
 */
int Jogador::getTotal () {
    return total;
}

/**
 * @details O metodo modifica o total de pontos do jogador, somando com o total adquirido na rodada
 * @param t Pontos do jogador conseguidos na rodada 
 */
void Jogador::setTotal (int t) {
    total += t;
}

/**
 * @return Se o jogador parou na rodada 
 */
bool Jogador::getParar () {
    return parar;
}

/**
 * @details O metodo modifica a decisao do jogador na rodada
  * @param p Decisao do jogador 
 */
void Jogador::setParar (bool p) {
    parar = p;
}

/**
 * @return Jogador excluido ou não da partida 
 */
bool Jogador::getExcluido () {
    return excluido;
}

/**
 * @details O metodo modifica se jogador foi excluido da partida
  * @param e Jogador foi excluido ou nao
 */
void Jogador::setExcluido (bool e) {
    excluido = e;
}

/** 
 * @details O operador e sobrecarregado para representar um jogador
 *			com os seus dados que ele necessita ver
 * @param	os Referencia para stream de saida
 * @param	j Referencia para o objeto Jogador a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream &os, Jogador &j) {

    os << "Jogador: " << j.nome << endl;
    os << "Pontos: " << j.total << endl << endl;
    
    return os;
}

/** 
 * @details O operador e sobrecarregado para realizar a atribuicao
 *			de um instante jogador a outro
 * @param	j Instante de jogador que sera atribuido ao objeto que invoca
 *			o metodo
 * @return	Autoreferencia para o objeto que invoca o metodo, com
 *			atributos atualizados
 */
Jogador& Jogador::operator = (Jogador const &j) {

     nome = j.nome;
     total = j.total;
     parar = j.parar;
     excluido = j.excluido;

     return *this;
 }