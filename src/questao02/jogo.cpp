/**
 * @file	jogo.cpp
 * @brief	Implementacao dos metodos da classe Jogo
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	07/05/2017
 * @date	07/05/2017
 * @sa		jogo.h
 */

#include <iostream>
using std::endl;
using std::cout;

#include <ostream>
using std::ostream;

#include <ctime>
using std::time;

#include <cstdlib>
using std::srand;

#include "jogo.h"

#define MAX_DADO 6          /** Valor máximo que um dado pode atingir */

/**
 * @details Os vetor de jogadores é inicializado vazio, os valores dos dados, num_jogadores e objetivo 
            sao iniciados com 0
 */
Jogo::Jogo () {
    jogadores = NULL;
    dado1 = 0;
    dado2 = 0;
    num_jogadores = 0;
    objetivo = 0;
}

/**
 * @return Valor do ojetivo do jogo 
 */
int Jogo::getObjetivo () {
    return objetivo;
}

/**
 * @details O metodo modifica o valor do objetivo do jogador
 * @param o Pontuacao objetivo para o jogo
 */
void Jogo::setObjetivo (int o) {
    objetivo = o;
}

/**
 * @details O metodo aloca o vetor de jogadores com o tamanho do numero de jogadores
 * @param n Numero de jogadores
 */
void Jogo::AlocaJogadores (int n) {

    delete [] jogadores;

    jogadores = new Jogador [n];
}

/**
 * @details O metodo modifica o nome do jogador e aumenta o numero de jogadores
 * @param n Nome do jogador
 */
void Jogo::setJogadores (string n) {
    jogadores[num_jogadores].setNome (n);

    num_jogadores += 1;
}

/** 
 * @details O operador e sobrecarregado para representar um jogo
 *			com os seus dados e os dados dos jogadores
 * @param	os Referencia para stream de saida
 * @param	j Referencia para o objeto Jogo a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream &os, Jogo &j) {
    
    os << endl;
    os << "******************************************************" << endl;
    os << "              Objetivo do jogo: " << j.objetivo << endl << endl;; 

    for (int ii = 0; ii < j.num_jogadores; ii++)
    {
        os << j.jogadores[ii];
    }

    os << "******************************************************" << endl << endl;
    return os;
}

/**
 * @details O metodo verifica se um jogador ultrapassou a pontuacao objetivo e se tiver o exclui do jogo,
 *          modificando o seu dado excluido para true e mostrando uma mensagem de que ele foi excluido
 * @param ii Indice do jogador verificado
 */
void Jogo::VerificaJogadores (int ii) {

    if (jogadores[ii].getTotal () > objetivo)
    {
        jogadores[ii].setExcluido (true);

        cout << endl;
        cout << "Jogador " << ii+1 << ": " << jogadores[ii].getNome () << endl;
        cout << "EXCLUIDO POR ULTRAPASSAR A PONTUAÇÃO OBJETIVO!!!" << endl;
    }
}

/**
 * @details O metodo se o jogo já acabou ou nao, observando se algum jogador atingiu a pontuacao objetivo do jogo,
 *          e se restou apenas um jogador na partida, declarando o campeao
 * @return Se o jogo acabou ou não
 */
bool Jogo::Acaba () {
    
    int cont = 0;

    for (int ii = 0; ii < num_jogadores; ii++)
    {
        if (jogadores[ii].getTotal () == objetivo)
        {
            campeao = jogadores[ii];

            return true;
        }
            
        if (jogadores[ii].getExcluido () == true)
            cont++;
    }

    if (cont >= num_jogadores-1)
    {
        for (int ii = 0; ii < num_jogadores; ii++)
            if (jogadores[ii].getExcluido () == false)
                campeao = jogadores[ii];

        return true;
    }

    return false;
}

/**
 * @details O metodo faz o lançamento de dois dados aleatoriamente com valores de 1 a 6, colocando seus valores
 *          em dado1 e dado2, e atribui a soma ao pontos do jogador que lançou os dados
 * @param ii  Indice do jogador que lançara os dados
 */
void Jogo::Rodada (int ii) {

    srand(time(NULL));

    dado1 = rand() % MAX_DADO + 1;
    dado2 = rand() % MAX_DADO + 1;

    cout << endl;
    cout << "Valor dado 1: " << dado1 << endl;
    cout << "Valor dado 2: " << dado2 << endl;

    int aux = dado1 + dado2;

    jogadores[ii].setTotal (aux);
}

/**
 * @param ii Indice do jogador para verificacao
 * @return Se o jogador foi excluido ou nao da partida
 */
bool Jogo::Excluido (int ii) {
    return jogadores[ii].getExcluido ();
}

/**
 * @details O metodo modifica o valor de 'parar' do Jogador, se ele deciciu parar na rodada
 * @param ii Indice do jogador que desejou nao jogar na rodada
 */
void Jogo::JogadorParou (int ii) {
    jogadores[ii].setParar (true);
}

/**
 * @details O metodo modifica o valor de 'parar' de todos os jogadores para false
 */
void Jogo::ResetParou () {
    for (int ii = 0; ii < num_jogadores; ii++)
        jogadores[ii].setParar (false);
}

/**
 * @details O metodo verifica qual jogador ficou mais perto da pontuacao objetivo no fim do jogo, 
            e declara o campeao caso ainda nao tenha sido declarado
 */
void Jogo::MaisProximo () {

    campeao = jogadores[0];

    for (int ii = 0; ii < num_jogadores; ii++)
    {
        if (campeao.getTotal () < jogadores[ii].getTotal () &&  jogadores[ii].getTotal () < objetivo)
            campeao = jogadores[ii];
    }    
}

/**
 * @details O metodo mostra os dados do jogador campeao da partida
*/
void Jogo::MostraCampeao () {
    cout << campeao;
}

/**
 * @details O vetor de jogadores é liberado se alocado com valores maiores que zero
 */
Jogo::~Jogo () {
    if (num_jogadores > 0)
        delete [] jogadores;
}