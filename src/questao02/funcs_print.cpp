/**
* @file     funcs_print.cpp
* @brief    Implementacao das funções que imprimem algumas mensangens aos usuarios do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since    07/05/2017
* @date	    07/05/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include "jogo.h"

#include "funcs_print.h"

/**
* @brief Função que imprime uma mensagem inicial no programa
*/
void imprimi_inicial () 
{
    cout << endl;
    cout << "       ▒█▀▀▄ ░▀░ ▀█░█▀ ░▀░ █▀▀█ ▀▀█▀▀ █▀▀█ ░░ █▀▀ █▀▀ 　 █▀▀ █▀▀█ █▀▄▀█ 　 █▀▀█ 　 ░░▀ █▀▀█ █▀▀▀ █▀▀█ " << endl;
    cout << "       ▒█░▒█ ▀█▀ ░█▄█░ ▀█▀ █▄▄▀ ░░█░░ █▄▄█ ▀▀ ▀▀█ █▀▀ 　 █░░ █░░█ █░▀░█ 　 █░░█ 　 ░░█ █░░█ █░▀█ █░░█ " << endl;
    cout << "       ▒█▄▄▀ ▀▀▀ ░░▀░░ ▀▀▀ ▀░▀▀ ░░▀░░ ▀░░▀ ░░ ▀▀▀ ▀▀▀ 　 ▀▀▀ ▀▀▀▀ ▀░░░▀ 　 ▀▀▀▀ 　 █▄█ ▀▀▀▀ ▀▀▀▀ ▀▀▀▀ " << endl << endl;
}

/**
* @brief Função que imprime a pontuacao objetivo e a pontuacao de cada jogador na partida
* @param jogo Informações da partida e dos jogadores
*/
void imprimi_pontuacao (Jogo &jogo)
{
    cout << endl << endl;
    cout << "       ▒█▀▀█ █▀▀█ █▀▀▄ ▀▀█▀▀ █░░█ █▀▀█ █▀▀ █▀▀█ █▀▀█ " << endl; 
    cout << "       ▒█▄▄█ █░░█ █░░█ ░░█░░ █░░█ █▄▄█ █░░ █▄▄█ █░░█ " << endl;
    cout << "       ▒█░░░ ▀▀▀▀ ▀░░▀ ░░▀░░ ░▀▀▀ ▀░░▀ ▀▀▀ ▀░░▀ ▀▀▀▀ "  << endl << endl;

    cout << jogo;
}

/**
* @brief Função que imprime o campeão da partida
* @param jogo Dados da partida e dos jogadores
*/
void imprimi_campeao (Jogo &jogo)
{
    cout << endl << endl;
    cout << "       ▒█▀▀█ █▀▀█ █▀▄▀█ █▀▀█ █▀▀ █▀▀█ █▀▀█ " << endl; 
    cout << "       ▒█░░░ █▄▄█ █░▀░█ █░░█ █▀▀ █▄▄█ █░░█ " << endl;
    cout << "       ▒█▄▄█ ▀░░▀ ▀░░░▀ █▀▀▀ ▀▀▀ ▀░░▀ ▀▀▀▀ "  << endl << endl;
    
    cout << endl;
    cout << "******************************************************" << endl << endl;
    jogo.MostraCampeao ();
    cout << "******************************************************" << endl;   
}