/**
* @file main2.cpp
* @brief Programa que executa a partida de um jogo
* @author Pedro Emerick (p.emerick@live.com)
* @since 06/04/17
* @date 11/04/17
*/

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <string>
using std::string;

#include <sstream>
using std::ws;

#include "jogo.h"
#include "funcs_print.h"
#include "func_rodada.h"

/** 
 * @brief Função principal
 * @param argc Possui o número de argumentos com os quais a função main() foi chamada na linha de comando
 * @param argv Cada string desta matriz é um dos parâmetros da linha de comando
 */
int main (int argc, char* argv[])
{
    /** Verifica se foi passado a pontuacao objetivo pela linha de comando */
    if (argc != 2)
    {
        cout << "Passe o valor objetivo/total do jogo pela linha de comando !!!" << endl;
        cout << "Exemplo: ./bin/jogo 10" << endl;

        return -1;
    }

    imprimi_inicial ();

    Jogo jogo;

    jogo.setObjetivo (atoi (argv[1]));

    int num_jogadores;

    /** Ler o numero de jogadores que tera na partida */
    do 
    {   
        cout << "Quantos jogadores irão participar: " << endl;
        cout << "--> ";
        cin >> num_jogadores;

        if (num_jogadores < 2)
            cout << endl << "A partida deve ter no minimo dois jogadores !!!" << endl << endl;
    }
    while (num_jogadores < 2);

    jogo.AlocaJogadores (num_jogadores);

    string nome_jogador;

    cout << endl;

    /** Leitura dos nomes dos jogadores */
    for (int ii = 1; ii <= num_jogadores; ii++)
    {
        cout << "Digite o nome do Jogador " << ii << ": ";
        getline (cin >> ws, nome_jogador);

        jogo.setJogadores (nome_jogador);
    }

    rodadas (jogo, num_jogadores);

    imprimi_pontuacao (jogo);

    imprimi_campeao (jogo);

    return 0;
}