/**
* @file     func_rodada.cpp
* @brief    Implementacao da função que executa a partida
* @author   Pedro Emerick (p.emerick@live.com)
* @since    07/05/2017
* @date	    07/05/2017
*/

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include "jogo.h"

#include "func_rodada.h"

/**
* @brief Função que executa a partida do jogo, recebendo a opcao de cada jogador e parando caso seja declarado um campeao
* @param jogo Dados da partida iniciada
* @param num_jogadores Quantidade de jogadores na partida
*/
void rodadas (Jogo &jogo, int num_jogadores)
{
    bool fim = false;
    
    int parar = 0;

    int num_rodada = 1;

    while (fim == false)
    {
        jogo.ResetParou ();

        cout << endl;
        cout << "********************** Rodada " << num_rodada << " **********************" << endl;

        for (int ii = 0; ii < num_jogadores; ii++)
        {
            if (jogo.Excluido (ii) != true)
            {
                int op;

                cout << endl;
                cout << "Jogador " << ii+1 << " deseja: " << endl;
                cout << "1) Jogar" << endl;
                cout << "2) Parar" << endl << endl;
                cout << "--> ";
                cin >> op;

                if (op == 2)
                {
                    jogo.JogadorParou (ii);

                    parar ++;
                }    
                else
                {
                    jogo.Rodada (ii);

                    jogo.VerificaJogadores (ii);

                    fim = jogo.Acaba ();
                }
            }
        }

        /** Acaba a partida caso todos jogadores nao quiseram jogar naquela rodada */
        if (parar == num_jogadores)
        {
            jogo.MaisProximo ();

            break;
        }
        
        parar = 0;

        num_rodada++;
    }
}