/**
 * @file	funcionario.h
 * @brief	Definicao da classe Funcionario, que representa um funcionario
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	03/05/2017
 * @date	06/05/2017
 */

#ifndef FUNCIONARIO_H
#define FUNCIONARIO_H

#include <string>
using std::string;

#include <ostream>
using std::ostream;

/**
 * @class   Funcionario funcionario.h
 * @brief   Classe que representa um funcionário
 * @details Os atributos de um funcionario são o nome, o salario, o dia, mes, e ano da admissao do funcionario
 */ 
class Funcionario {
    private:
        string nome;            /**< Nome do funcionario */
        float salario;          /**< Salario do funcionario */
        int dia;                /**< Dia de admissao do funcionario */
        int mes;                /**< Mes de admissao do funcionario */
        int ano;                /**< Ano de admissao do funcionario */
    public:
        /**< @brief Construtor padrao */
        Funcionario ();
        
        /**< @brief Construtor parametrizado */
        Funcionario (string n, float s, int d, int m, int a);

        /**< @brief Retorna o nome do funcionario */
        string getNome ();

        /**< @brief Modifica o nome do funcionario */  
        void setNome (string n);

        /**< @brief Retorna o salario do funcionario */
        float getSalario ();

        /**< @brief Modifica o salario do funcionario */  
        void setSalario (float s);
        
        /**< @brief Retorna o dia de admissao do funcionario */        
        int getDia ();

        /**< @brief Modifica o dia de admissao do funcionario */        
        void setDia (int d);

        /**< @brief Retorna o mes de admissao do funcionario */
        int getMes ();

        /**< @brief Modifica o mes de admissao do funcionario */  
        void setMes (int m);

        /**< @brief Retorna o ano de admissao do funcionario */
        int getAno ();

        /**< @brief Modifica o ano de admissao do funcionario */  
        void setAno (int a);

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream &os, Funcionario &f);

        /** @brief Sobrecarga do operador de igualdade */        
        bool operator == (Funcionario f);

        /**< @brief Retorna se o funcionario esta em tempo de experiencia */
        bool Experiencia (int d, int m, int a);
};

#endif