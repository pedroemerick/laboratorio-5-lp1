/**
* @file     menus.h
* @brief    Declaracao dos prototipos das funcoes que imprimem os menus do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    03/05/2017
* @date	    06/05/2017
*/

#ifndef MENUS_H
#define MENUS_H

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @param menu Valor do menu
* @return Opcao escolhida pelo usuario
*/
int menu_principal (int menu);

/**
* @brief Função que imprime o menu para a adicao de funcionarios com suas opcoes e recebe a escolha do usuario
* @param menu2 Valor do menu para a adicao de funcionarios
* @return Opcao escolhida pelo usuario
*/
int menu_add_funcionario (int menu2);

/**
* @brief Função que imprime o menu para carregar uma empresa atraves de um arquivo 
         com suas opcoes e recebe a escolha do usuario
* @param opcao Valor da opcao
* @return Opcao escolhida pelo usuario
*/
int menu_inicial (int opcao);

#endif