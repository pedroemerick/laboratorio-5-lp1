/**
* @file     funcs_dados.h
* @brief    Declaracao dos prototipos de algumas das funções que modificam os dados do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since    03/05/2017
* @date	    06/05/2017
*/

#ifndef FUNCS_DADOS_H
#define FUNCS_DADOS_H

#include "empresa.h"

/**
* @brief Função que busca uma empresa pelo cnpj
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
* @return Indice de onde se encontra a empresa
*/
int busca_empresa (Empresa *emp, int num_empresa);

/**
* @brief Função que aloca o vetor de empresas com o seu tamanho mais um
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
* @return Vetor de empresas com mais um espaço para empresa
*/
Empresa* nova_empresa (Empresa *emp, int *num_empresa);

/**
* @brief Função que cria uma nova empresa, recebendo seus dados e atribuindo na empresa
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
*/
void cadastra_empresa (Empresa *emp, int num_empresa);

/**
* @brief Função que adiciona funcionario em uma empresa pelo terminal ou por um arquivo
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
* @param data Data atual quando iniciado o programa
*/
void add_funcionario (Empresa *emp, int num_empresa, struct tm *data);

/**
* @brief Função que carrega os dados de uma empresa e os dados dos seus funcionarios atraves de um arquivo
* @param emp Vetor com empresas
* @param num_empresa Quantidade de empresas existentes
*/
Empresa* carrega_empresa (Empresa *emp, int *num_empresa);

#endif