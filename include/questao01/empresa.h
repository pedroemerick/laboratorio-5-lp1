/**
 * @file	empresa.h
 * @brief	Definicao da classe Empresa, que representa uma empresa
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	03/05/2017
 * @date	06/05/2017
 */

#ifndef EMPRESA_H
#define EMPRESA_H

#include "funcionario.h"

#include <string>
using std::string;

#include <ostream>
using std::ostream;

/**
 * @class   Empresa empresa.h
 * @brief   Classe que representa uma empresa
 * @details Os atributos de uma empresa são o nome, o cnpj, os dados dos funcionários e o número de funcionarios
 */  
class Empresa {
    private:
        string nome;                        /**< Nome da empresa */
        long int cnpj;                      /**< Cnpj da empresa */
        Funcionario *empregados;            /**< Dados dos funcionários */
        int num_empregados;                 /**< Números de funcionários da empresa */
    public:
        /**< @brief Construtor padrao */
        Empresa ();                         

        /**< @brief Retorna o nome da empresa */
        string getNomeEmpresa (); 

        /**< @brief Modifica o nome da empresa */          
        void setNomeEmpresa (string n);     

        /**< @brief Retorna o cnpj da empresa */
        long int getCnpj ();    

        /**< @brief Modifica o cnpj da empresa */            
        void setCnpj (long int c);

        /**< @brief Retorna o número de funcionários da empresa */
        int getNumEmpregados ();            

        /**< @brief Modifica o tamanho do vetor de funcionários */
        void setNovoFuncionario (int n);    

        /**< @brief Inseri os dados de um funcionário */
        void setEmpregados (string n, float s, int d, int m, int a);    

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream &os, Empresa &e);

        /**< @brief Modifica o salário dos funcionários com um aumento */
        void setAumento (int p);

        /**< @brief Mostra os funcinários em tempo de experiência */        
        void FuncionarioExperiencia (int d, int m, int a);

        /**< @brief Salva as empresas em um arquivo com o nome da empresa */        
        void Salvar ();

        /**< @brief Verifica se um determinado funcionário existe */        
        bool ExisteFuncionario (string n, float s, int d, int m, int a);

        /** @brief Sobrecarga do operador de atribuição */        
        Empresa& operator = (Empresa const &e);

        /**< @brief Destrutor padrão */
        ~Empresa ();
};

#endif