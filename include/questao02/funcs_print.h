/**
* @file     funcs_print.h
* @brief    Declaracao dos prototipos das funções que imprimem algumas mensangens aos usuarios do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since    07/05/2017
* @date	    07/05/2017
*/

#ifndef FUNCS_PRINT_H
#define FUNCS_PRINT_H

#include "jogo.h"

/**
* @brief Função que imprime uma mensagem inicial no programa
*/
void imprimi_inicial ();

/**
* @brief Função que imprime a pontuacao objetivo e a pontuacao de cada jogador na partida
* @param jogo Informações da partida e dos jogadores
*/
void imprimi_pontuacao (Jogo &jogo);

/**
* @brief Função que imprime o campeão da partida
* @param jogo Dados da partida e dos jogadores
*/
void imprimi_campeao (Jogo &jogo);

#endif