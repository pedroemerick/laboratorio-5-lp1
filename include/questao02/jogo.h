/**
 * @file	jogo.h
 * @brief	Definicao da classe Jogo, que representa um Jogo
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	07/05/2017
 * @date	07/05/2017
 */

#ifndef JOGO_H
#define JOGO_H

#include <ostream>
using std::ostream;

#include "jogador.h"

/**
 * @class   Jogo jogo.h
 * @brief   Classe que representa um jogo
 * @details Os atributos de um jogo são os jogadores, o campeao da partida, os valores dos dados na rodada, 
            o numero de jogadores e o ponto que é o objetivo do jogo
 */ 
class Jogo {
    private:
        Jogador *jogadores;             /**< Dados dos jogadores no jogo */
        Jogador campeao;                /**< Dados do jogador campeão */
        int dado1, dado2;               /**< Valores do dados jogados */
        int num_jogadores;              /**< Numero de jogadores participantes do jogo */
        int objetivo;                   /**< Pontuação objetivo do jogo */
    public:
        /**< @brief Construtor padrao */
        Jogo ();

        /**< @brief Retorna o ponto objetivo do jogo */ 
        int getObjetivo ();

        /**< @brief Modifica o ponto objetivo do jogo */ 
        void setObjetivo (int o);

        /**< @brief Aloca o vetor de jogadores com o número de jogadores */ 
        void AlocaJogadores (int n);

        /**< @brief Modifica o nome dos jogadores */ 
        void setJogadores (string n);

        /**< @brief Verifica se o jogador deve ser excluido */ 
        void VerificaJogadores (int ii);

        /**< @brief Retorna se o jogo deve acabar */ 
        bool Acaba ();

        /**< @brief Modifica os valores dos dados e coloca na pontuação do jogador */ 
        void Rodada (int ii);

        /**< @brief Retorna se determinado jogador foi excluido do jogo */ 
        bool Excluido (int ii);

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream &os, Jogo &j);

        /** @brief Mostra o jogador campeao do jogo */
        void MostraCampeao ();

        /** @brief Mofica a decisao de um determinado jogador */
        void JogadorParou (int ii);

        /** @brief Reseta a decisao de todos os jogadores */
        void ResetParou ();

        /** @brief Escolhe o campeao atraves da pontuacao mais perto do objetivo do jogo */
        void MaisProximo ();

        /** @brief Destrutor padrão*/
        ~Jogo ();
};

#endif