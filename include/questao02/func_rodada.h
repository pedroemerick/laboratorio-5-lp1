/**
* @file     func_rodada.h
* @brief    Declaracao do prototipo da função que executa a partida
* @author   Pedro Emerick (p.emerick@live.com)
* @since    07/05/2017
* @date	    07/05/2017
*/

#ifndef FUNC_RODADA_H
#define FUNC_RODADA_H

#include "jogo.h"

/**
* @brief Função que executa a partida do jogo, recebendo a opcao de cada jogador e parando caso seja declarado um campeao
* @param jogo Dados da partida iniciada
* @param num_jogadores Quantidade de jogadores na partida
*/
void rodadas (Jogo &jogo, int num_jogadores);

#endif