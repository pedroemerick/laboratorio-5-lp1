/**
 * @file	jogador.h
 * @brief	Definicao da classe Jogador, que representa um Jogador par um determinado jogo
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	07/05/2017
 * @date	07/05/2017
 */

#ifndef JOGADOR_H
#define JOGADOR_H

#include <string>
using std::string;

#include <ostream>
using std::ostream;

/**
 * @class   Jogador jogador.h
 * @brief   Classe que representa um jogador
 * @details Os atributos de um jogador são o nome, o total de pontos alcançados, 
            se ele nao jogou na rodada e se foi excluído do jogo
 */ 
class Jogador {
    private:
        string nome;            /**< Nome do jogador */
        int total;              /**< Total de pontos ja alcançados pelo jogador */
        bool parar;             /**< Se o jogador opctou por nao jogar na rodada */
        bool excluido;          /**< Se o jogador doi excluido da partida */

    public: 
    /**< @brief Construtor padrao */
    Jogador ();

    /**< @brief Retorna o nome do jogador */    
    string getNome ();

    /**< @brief Modifica o nome do jogador */
    void setNome (string n);

    /**< @brief Retorna os pontos do jogador */
    int getTotal ();
    
    /**< @brief Modifica os pontos do jogador */
    void setTotal (int t);

    /**< @brief Retorna a decisão do jogador na rodada */
    bool getParar ();

    /**< @brief Modifica a decisão do jogador */
    void setParar (bool p);

    /**< @brief Retorna se o jogador foi excluido */
    bool getExcluido ();

    /**< @brief Modifica se o jogador foi excluido */
    void setExcluido (bool e);

    /** @brief Sobrecarga do operador de insercao em stream */
    friend ostream& operator << (ostream &os, Jogador &j);

    /** @brief Sobrecarga do operador de atribuicao */
    Jogador& operator = (Jogador const &j);
};

#endif
