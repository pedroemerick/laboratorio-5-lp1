LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc
TEST_DIR = ./test

CC = g++
CPPFLAGS = -Wall -pedantic -ansi -std=c++11

RM = rm -rf
RM_TUDO = rm -fr

.PHONY: all clean debug doc doxygen valgrind1 valgrind2	

all: init questao01 questao02

debug: CPPFLAGS += -g -O0
debug: all

valgrind1: 
	valgrind --leak-check=full --show-reachable=yes -v ./bin/empresa

valgrind2: 
	valgrind --leak-check=full --show-reachable=yes -v ./bin/jogo 10

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/questao01/
	@mkdir -p $(OBJ_DIR)/questao02/

# QUESTAO 01

questao01: CPPFLAGS += -I. -I$(INC_DIR)/questao01
questao01: $(OBJ_DIR)/questao01/main.o $(OBJ_DIR)/questao01/funcionario.o $(OBJ_DIR)/questao01/empresa.o $(OBJ_DIR)/questao01/funcs_dados.o $(OBJ_DIR)/questao01/menus.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/empresa $^
	@echo "*** [Executavel empresa criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao01/main.o: $(SRC_DIR)/questao01/main.cpp $(INC_DIR)/questao01/empresa.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao01/funcionario.o: $(SRC_DIR)/questao01/funcionario.cpp $(INC_DIR)/questao01/funcionario.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao01/empresa.o: $(SRC_DIR)/questao01/empresa.cpp $(INC_DIR)/questao01/empresa.h $(INC_DIR)/questao01/funcionario.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao01/funcs_dados.o: $(SRC_DIR)/questao01/funcs_dados.cpp $(INC_DIR)/questao01/funcs_dados.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao01/menus.o: $(SRC_DIR)/questao01/menus.cpp $(INC_DIR)/questao01/menus.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# QUESTAO 02

questao02: CPPFLAGS += -I. -I$(INC_DIR)/questao02
questao02: $(OBJ_DIR)/questao02/main2.o $(OBJ_DIR)/questao02/jogador.o $(OBJ_DIR)/questao02/jogo.o $(OBJ_DIR)/questao02/funcs_print.o $(OBJ_DIR)/questao02/func_rodada.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/jogo $^
	@echo "*** [Executavel jogo criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao02/main2.o: $(SRC_DIR)/questao02/main2.cpp $(INC_DIR)/questao02/jogo.h $(INC_DIR)/questao02/funcs_print.h $(INC_DIR)/questao02/func_rodada.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao02/jogador.o: $(SRC_DIR)/questao02/jogador.cpp $(INC_DIR)/questao02/jogador.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao02/jogo.o: $(SRC_DIR)/questao02/jogo.cpp $(INC_DIR)/questao02/jogo.h $(INC_DIR)/questao02/jogador.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao02/funcs_print.o: $(SRC_DIR)/questao02/funcs_print.cpp $(INC_DIR)/questao02/funcs_print.h $(INC_DIR)/questao02/jogo.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao02/func_rodada.o: $(SRC_DIR)/questao02/func_rodada.cpp $(INC_DIR)/questao02/func_rodada.h $(INC_DIR)/questao02/jogo.h
	$(CC) -c $(CPPFLAGS) -o $@ $<


doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR), $(OBJ_DIR) e $(IMG_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/questao01/*
	$(RM) $(OBJ_DIR)/questao02/*
